# coding: utf-8
from contextlib import closing
from itertools import islice
import sqlite3


def get_database_connection(filepath):
    """Возвращает подключение к базе данных сайта.

    :param str filepath: Путь к файлу базы данных.

    :rtype: sqlite3.Connection
    """
    return sqlite3.connect(str(filepath))


def get_photos_count(dbc):
    """Возвращает количество фотографий в фотогаллерее.

    :param dbc: подключение к базе данных сайта.
    :type dbc: sqlite3.Connection

    :rtype: int
    """
    with closing(dbc.execute('SELECT count(*) FROM photos')) as cursor:
        return cursor.fetchone()[0]


def get_photos_info(dbc):
    """Возвращает информацию о фотографиях в фотогаллерее.

    На каждой итерации генератор возвращает словарь с двумя ключами:

        * ``url`` - относительный адрес фотографии на сервере;
        * ``title`` - название фотографии.

    :param dbc: подключение к базе данных сайта.
    :type dbc: sqlite3.Connection

    :rtype: generator
    """
    with closing(dbc.execute('SELECT filename, title FROM photos')) as cursor:
        for filename, title in cursor:
            yield dict(
                url=filename,
                title=title,
            )


def get_rows(photos_info, photos_count, column_count):
    """Возвращает информацию о фотографиях с разбивкой по строкам.

    :param _info: информация о фотографиях в фотогаллерее.
    :type files_info: collections.abc.Iterable

    :param int column_count: количество колонок.

    :rtype: generator
    """
    for _ in range(photos_count // column_count + 1):
        yield islice(photos_info, 0, column_count)
